"use strict";
const querystring = require('querystring');
const fetch = require('node-fetch');

//  GetLocations Method
export const postLocationsByPosition = (req, res) => {

    const coords = req.body.coordinates;

    const query = querystring.stringify({
        key: process.env.GOOGLE_MAPS_KEY,
        location: `${coords.lat},${coords.lng}`,
        radius: 2000,
        fields: 'formatted_address,name,photos',
        type: 'atm'
    });

    const url = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?${query}`;

    fetch(url)
        .then(response => response.json())
        .then(data => {
            let placesData = [];
            data.results.forEach(element => {
                placesData.push(
                    {
                        name: element.name,
                        photos: element.photos,
                        opening_hours: element.opening_hours,
                        address: element.vicinity
                    }
                );
            });
            res.set("Content-Type", "text/json");
            res.set(200);
            res.json(placesData);
            res.end();
        })
        .catch(err => {
            console.log('error', err);
            //  Next Time Could be
        });

};


//type: 'atm'