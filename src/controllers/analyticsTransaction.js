import Query from '../models/query';

const transactionsModel = new Query();


export const transactionPage = async (req, res) => {
    try {
        const data = await transactionsModel.statement("SELECT NAME, VALUE FROM TRANSACTION");
        res.status(200).json({ messages: data.rows });
    } catch (err) {
        res.status(200).json({ messages: err.stack });
    }
};