import express from 'express';
import { indexPage, transactionPage, postLocationsByPosition } from '../controllers';

const indexRouter = express.Router();

//  Locations Router, Middleware to get Near Locations
indexRouter.get('/trans', transactionPage);
indexRouter.post('/locations', postLocationsByPosition);
indexRouter.get('/', indexPage);


export default indexRouter;