import {pool} from './pool';

class Query{
    constructor(){
        this.pool = pool;
        this.pool.on('error', (err, client) => `Error, ${err}, on idle client${client}`);
    }

    async statement(str) {
        return this.pool.query(str);
    }
}

export default Query;